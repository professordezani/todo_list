// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

class Cadastro extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nova Tarefa"),
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.all(10),
            child: TextField(
              keyboardType: TextInputType.text,
              enabled: true,
              obscureText: false,
              decoration: InputDecoration(
                hintText: "Descrição da Tarefa",
                labelText: "Descrição da Tarefa"
              ),           
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 20,
            child: ElevatedButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("Salvar"),
            ),
          ),
        ],
      )
    );
  }
}