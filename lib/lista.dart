// ignore_for_file: prefer_const_constructors
// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'controllers/taskcontroller.dart';

class Lista extends StatelessWidget {
  final taskCtrl = TaskController();
  final db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Todo List"),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: db.collection('tasks').snapshots(),
        builder: (_, snapshot) {

          if (!snapshot.hasData)            
            return Center(
              child: CircularProgressIndicator(),
            );

          var documents = snapshot.data!.docs;

          return ListView.builder(
              itemCount: snapshot.data!.docs.length,
              itemBuilder: (_, index) {
                var task = documents[index].data()! as Map<String, dynamic>;
                return Item(task['texto'], task['concluida']);
              });
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed('/cadastro'),
        child: Icon(Icons.add),
      ),
    );
  }
}

class Item extends StatefulWidget {
  // atributos:
  String texto;
  bool concluida;

  //construtor:
  Item(this.texto, this.concluida);

  @override
  State<Item> createState() => _ItemState();
}

class _ItemState extends State<Item> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 5, 10, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(widget.texto),
          Checkbox(
            value: widget.concluida,
            onChanged: (value) => setState(() => widget.concluida = value!),
          ),
        ],
      ),
    );
  }
}
