class Task {

  // atributos:
  String texto;
  bool concluida;

  // construtor:
  Task(this.texto, this.concluida);
}

// Task t1 = new Task('Texto1', true);
// Task t2 = new Task('Texto2', true);
// var tasks = [t1, t2];